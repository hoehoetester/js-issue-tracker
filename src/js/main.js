import { chence } from '../assets/chance';

const OPEN = 'Open';
const CLOSED = 'Closed';

document.getElementById('issueInputForm').addEventListener('submit', saveIssue);

// set issues to localStorage
function setIssuesToLocalStorage(issues) {
    localStorage.setItem('issues', JSON.stringify(issues));
}

// get issues from localStorage
function getIssuesFromLocalStorage() {
    return JSON.parse(localStorage.getItem('issues'));
}

// fetch issues
function fetchIssues() {
    const issues = getIssuesFromLocalStorage();
    var issuesListe = document.getElementById('issuesList');
    issuesList.innerHTML = '';

    for (var i = 0; i < issues.length; i++) {
        var id = issues[i].id;
        var desc = issues[i].description;
        var priority = issues[i].priority;
        var assignedTo = issues[i].assignedTo;
        var status = issues[i].status;

        issuesList.innerHTML += `
            <div class="well">
            <h5>Issue ID: ${ id } <span class="label label-info pull-right"> ${ status } </span></h5>
            <h3> ${ desc } </h3>
            <p><span class="glyphicon glyphicon-time"></span> ${ priority }</p>
            <p><span class="glyphicon glyphicon-user"></span> ${ assignedTo }</p>
            <button onclick="setStatusClosed('${ id }')" class="btn btn-success">Open / Close</button>
            <button onclick="deleteIssue('${ id }')" class="btn btn-danger">Delete</button>
            </div>`;
    }
}
window.fetchIssues = fetchIssues;

// save issue
function saveIssue(e) {
    var issueDesc = document.getElementById('issueDescInput').value;
    var issuePriority = document.getElementById('issuePriorityInput').value;
    var issueAssignedTo = document.getElementById('issueAssignedToInput').value;
    var issueId = chance.guid();
    var issueStatus = OPEN;

    var issue = {
        id: issueId,
        description: issueDesc,
        priority: issuePriority,
        assignedTo: issueAssignedTo,
        status: issueStatus
    }

    if (localStorage.getItem('issues') == null) {
        var issues = [];
        issues.push(issue);
        setIssuesToLocalStorage(issues);
    } else {
        var issues = getIssuesFromLocalStorage();
        issues.push(issue);
        setIssuesToLocalStorage(issues);
    }

    document.getElementById('issueInputForm').reset();
    fetchIssues();
    e.preventDefault();
}

// close issues
function setStatusClosed(id) {
    var issues = getIssuesFromLocalStorage();

    for (var i = 0; i < issues.length; i++) {
        if (issues[i].id == id) {
            issues[i].status == OPEN ? issues[i].status = CLOSED : issues[i].status = OPEN;
        }
    }

    setIssuesToLocalStorage(issues);
    fetchIssues();
}
window.setStatusClosed = setStatusClosed;

// delete issues
function deleteIssue(id) {
    var del = confirm('Are you sure?');
    if (del) {
        var issues = getIssuesFromLocalStorage();

        for (var i = 0; i < issues.length; i++) {
            if (issues[i].id == id) {
                issues.splice(i, 1);
            }
        }

        setIssuesToLocalStorage(issues);
        fetchIssues();
    }
}
window.deleteIssue = deleteIssue;